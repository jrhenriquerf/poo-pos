<?php

require 'Calculo.php';
require 'Conta.php';

class CalculadoraConta implements Calculo
{
    private $contaUm;
    private $contaDois;

    public function __construct(Conta $contaUm, Conta $contaDois)
    {
        $this->contaUm = $contaUm;
        $this->contaDois = $contaDois;
    }

    public function soma(): integer
    {
        return $this->contaUm->getSaldo() + $this->contaDois->getSaldo();
    }

    public function subtracao(): double
    {
        return $this->contaUm->getSaldo() - $this->contaDois->getSaldo();
    }

    public function divisao(): double
    {
        return $this->contaUm->getSaldo() / $this->contaDois->getSaldo();
    }

    public function multiplicacao(): double
    {
        return $this->contaUm->getSaldo() * $this->contaDois->getSaldo();
    }
}