<?php

interface Calculo
{
    public function soma(): integer;
    public function subtracao(): double;
    public function divisao(): double;
    public function multiplicacao(): double;
}