<?php

abstract class Conta
{
    protected $saldo;

    public function __construct()
    {
        $this->saldo = 0.0;
    }

    public function saca(float $valor): bool
    {
        if ($this->getSaldo() < $valor) {
            return false;
        }

        $this->saldo -= $valor;
        return true;
    }

    public function deposita(float $valor): void
    {
        $this->saldo += $valor;
    }

    public abstract function getSaldo(): float;
}
