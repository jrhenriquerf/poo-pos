<?php

require_once('Conta.php');

class ContaEstudante extends Conta
{
    public function getSaldo(): float
    {
        return $this->saldo - ( $this->saldo * 0.05 );
    }
}
