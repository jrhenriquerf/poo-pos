<?php

// Contas
require 'Cliente.php';
require 'ContaCorrente.php';
require 'ContaEmpresarial.php';
require 'ContaEstudante.php';
require 'ContaConjunta.php';

// Calculadora
require 'CalculadoraConta.php';

$cliente = new Cliente(new ContaCorrente(), 'João');
$cliente->conta->deposita((float) 10.0);

$clienteDois = new Cliente(new ContaConjunta(), 'Paulo');
$clienteDois->conta->deposita((float) 25.0);

$calculadora = new CalculadoraConta($cliente->conta, $clienteDois->conta);
var_dump($calculadora->soma());
