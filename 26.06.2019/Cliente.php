<?php

require 'Conta.php';

class Cliente
{
    private $nome;
    public $conta;

    public function __construct(Conta $conta, string $nome)
    {
        $this->nome = $nome;
        $this->conta = $conta;
    }

    public function setName(string $nome): void
    {
        $this->nome = $nome;
    }

    public function getNome(): string
    {
        return $this->nome;
    }
}