<?php

// Contas
require 'Cliente.php';
require 'ContaCorrente.php';
require 'ContaEstudante.php';

$conta = new ContaCorrente(new Cliente('João'));
$contaDois = new ContaEstudante(new Cliente('Paulo'));

$conta->deposita((float) 10.0);
$contaDois->deposita((float) 25.0);

echo "Saldo da conta 1: " . $conta->getSaldo() . "\n";
echo "Saldo da conta 2: " . $contaDois->getSaldo();
