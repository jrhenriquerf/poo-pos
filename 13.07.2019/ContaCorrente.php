<?php

require_once('Conta.php');
require_once('Rendimento.php');

class ContaCorrente extends Conta implements Rendimento
{
    public function getSaldo(): float
    {
        return $this->saldo - ( $this->saldo * 0.2 );
    }

    public function rendimento()
    {
        $this->saldo *= 1.1;
    }
}