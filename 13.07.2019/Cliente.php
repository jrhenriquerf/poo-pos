<?php

class Cliente
{
    private $nome;

    public function __construct(string $nome)
    {
        $this->nome = $nome;
    }

    public function setName(string $nome): void
    {
        $this->nome = $nome;
    }

    public function getNome(): string
    {
        return $this->nome;
    }
}