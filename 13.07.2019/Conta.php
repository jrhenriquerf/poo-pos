<?php

abstract class Conta
{
    protected $saldo;
    private $cliente;

    public function __construct(Cliente $cliente)
    {
        $this->saldo = 0.0;
        $this->$cliente = $cliente;
    }

    public function saca(float $valor): bool
    {
        if (!podeSacar($valor)) {
            return false;
        }

        $this->saldo -= $valor;
        return true;
    }

    public function deposita(float $valor): void
    {
        $this->saldo += $valor;
    }

    public abstract function getSaldo(): float;

    private function podeSacar(double $valor) : boolean
    {
		return getSaldo() >= $valor;
	}
}
