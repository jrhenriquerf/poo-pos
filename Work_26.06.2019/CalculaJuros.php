<?php

require 'Juros.php';

class CalculaJuros
{
    public function calcular(Conta $conta, Juros $juros): float
    {
        return $juros->calculaJuros($conta->getSaldo());
    }
}